/*******************************************************************************
 * pixm
 *
 * xmpcr.c:  XMPCR communications module
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef XMPCR_H
#define XMPCR_H

/*========================================================================
 * Defined values
 *=======================================================================*/

#define BCD2DEC(x) (((x)>>4)*10+((x)&0xf))

/* How many should these be? Can they be retrieved dynamically? */
#define MAX_CATEGORIES      256
#define MAX_CHANNELS        256

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/

/*
 * Radio responses: each is 1 byte larger than the response, for the string terminator
 * Oversized fields are to quiet gcc!
 */
typedef struct _xmpcr_t
{
    char    sdec_version[20];   /* SDEC Version %d, %d/%d/%d (should be 15 bytes!) */
    char    cbm_version[20];    /* SDEC Version %d, %d/%d/%d (should be 15 bytes!) */
    char    xmstk_version[20];  /* XM STK Version %d, %d/%d/%d (should be 15 bytes!) */
    char    radio_id[9];        /* XM Radio ID: %s */
    char    goodbye[9];         /* Goodbye key: %02d %02d %02d */
    char    chan_num[5];        /* Channel number: 2 digits (repeated) */
    char    station[17];        /* Channel name: %s */
    char    category[17];       /* Category: %s */
    char    artist[17];         /* Artist: %s */
    char    title[17];          /* Song title: %s */
} XMPCR_T;

/*
 * Channel info for all channels
 */
typedef struct _chaninfo_t
{
    unsigned char   number;
    char            name[17];
    char            category[17];
    char            artist[17];
    char            title[17];
} CHANINFO_T;

/*
 * Channel info for all channels
 */
typedef struct _signalinfo_t
{
    char    satSignal;        // 0:none, 1:Fair, 2:Good, 3:Excellent
    char    antStatus;        // 0 = Not connected, 3 = connected
    char    terSignal;        // 0:none, 1:Fair, 2:Good, 3:Excellent

} SIGNALINFO_T;

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef XMPCR_C
extern char         mute;
extern XMPCR_T      xmpcr;
extern CHANINFO_T   channel_list[MAX_CHANNELS];
extern char         *category_list[MAX_CATEGORIES];

extern int  xmpcr_init( int );
extern int  xmpcr_power_on( void );
extern int  xmpcr_power_off( void );
extern void xmpcr_get_radio_id( void );
extern void xmpcr_get_info( void );
extern int  xmpcr_get_channel_info( unsigned char number, int dump_channel);
extern void xmpcr_get_signal_info( void );
extern void xmpcr_set_channel( int num );
extern void xmpcr_mute( void );
extern void dump_channels( void );
extern int  xmpcr_get_active_channel( void );
extern void xmpcr_close( void );
#endif /* !XMPCR_C */
#endif /* !XMPCR_H */
