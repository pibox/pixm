/*******************************************************************************
 * pixm
 *
 * pixm.h:  program main
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef PIXM_H
#define PIXM_H

#include <gtk/gtk.h>

#define KEYSYMS_F     "/etc/pibox-keysyms"
#define KEYSYMS_FD    "data/pibox-keysyms"

#define DB_CATEGORIES   0
#define DB_CHANNELS     1

/*========================================================================
 * Defined values
 *=======================================================================*/
#define PROG        "pixm"
#define MAXBUF      4096

// Where the config file is located
#define F_CFG   "/etc/pixm.cfg"
#define F_CFG_T "data/pixm.cfg"

#define MUTE_ON_PATH_T      "data/icons/mute-on.png"
#define MUTE_OFF_PATH_T     "data/icons/mute-off.png"
#define RADIO_ON_PATH_T     "data/icons/radio-on.png"
#define RADIO_OFF_PATH_T    "data/icons/radio-off.png"

#define MUTE_ON_PATH        "/etc/pixm/mute-on.png"
#define MUTE_OFF_PATH       "/etc/pixm/mute-off.png"
#define RADIO_ON_PATH       "/etc/pixm/radio-on.png"
#define RADIO_OFF_PATH      "/etc/pixm/radio-off.png"

/*========================================================================
 * Globals
 *=======================================================================*/
#ifdef PIXM_C
int     daemonEnabled = 0;
char    errBuf[MAXBUF];
#else
extern int      daemonEnabled;
extern char     errBuf[];
#endif /* PIXM_C */

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef PIXM_C
extern char radio_init;
extern GtkListStore *channel_cat_list_store;
extern void update_channel(int channel_num);
extern void update_list( void );
extern guint getChoice( void );
#endif

/*========================================================================
 * Include other headers
 *=======================================================================*/
#include <pibox/log.h>
#include "cli.h"
#include "utils.h"
#include "channelMgr.h"
#include "xmpcr.h"
#include "db.h"

#endif /* !PIXM_H */

