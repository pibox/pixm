/*******************************************************************************
 * pixm
 *
 * pixm.c:  program main
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define PIXM_C

/* To avoid deprecation warnings from GTK+ 2.x */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <uuid/uuid.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <pibox/utils.h>

#include "pixm.h"

#define CATEGORIES_S    "Categories"
#define CHANNEL_S       "Channels"

GtkWidget       *window;
GtkWidget       *menuArea;
GtkWidget       *darea;
GtkWidget       *view;
GtkWidget       *station[4];
GtkListStore    *channel_cat_list_store;
cairo_surface_t *mute_on_image = NULL;
cairo_surface_t *mute_off_image = NULL;
cairo_surface_t *radio_on_image = NULL;
cairo_surface_t *radio_off_image = NULL;

char        radio_init = 0;
guint       homeKey = -1;
GtkWidget   *window;
guint       mgrProcessorId = -1;

/*
 * DB Choice
 * 0: DB_CATEGORIES
 * 1: DB_CHANNELS
 */
guint db_choice = DB_CATEGORIES;
char *imagePath = NULL;
char *seriesImagePath = NULL;
char *overview = NULL;

char *station_labels[] = {
    "Station",
    "Category",
    "Artist",
    "Title",
};

/* Mutex for async updates. */
static pthread_mutex_t channelDisplayMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t menuDisplayMutex = PTHREAD_MUTEX_INITIALIZER;

/*
 * Function Prototypes
 */
void do_menu_drawing();
void entry_selected ();

/*
 *========================================================================
 * Name:   search_func
 * Prototype:  int search_func( void )
 *
 * Description:
 * Called when interactive search is used in the list of categories 
 * and channels.
 *
 * Returns:
 * 1 on success
 * 0 on failure
 *========================================================================
 */
gboolean
search_func(
        GtkTreeModel *model, 
        gint column, 
        const gchar *key, 
        GtkTreeIter *iter, 
        gpointer search_data)
{
    GValue  value = {0, };
    char    *entry_text;
    char    *ptr;

    piboxLogger(LOG_INFO, "Entered.\n");
    gtk_tree_model_get_value(GTK_TREE_MODEL(model), iter, column, &value);
    ptr = strdup(g_value_get_string(&value));
    entry_text = piboxTrim(ptr);

    piboxLogger(LOG_INFO, "Comparing *%s* to *%s*\n", key, entry_text);
    if ( strncasecmp(key, entry_text, strlen(key)) == 0 )
    {
        piboxLogger(LOG_INFO, "Found match.\n");
        g_value_unset(&value);
        free(ptr);
        return FALSE;
    }
    g_value_unset(&value);
    free(ptr);
    return TRUE;
}

/*
 *========================================================================
 * Name:   initRadio
 * Prototype:  int initRadio( void )
 *
 * Description:
 * Power up and initialize the radio.
 *
 * Returns:
 * 1 on success
 * 0 on failure
 *========================================================================
 */
static int initRadio( void )
{
    piboxLogger(LOG_INFO, "Default port: %s\n", cliOptions.port);
    if ( !xmpcr_init( TRUE ) )
    {
        piboxLogger(LOG_ERROR, "Failed to initialize radio.\n");
        return(0);
    }
    xmpcr_close();

    piboxLogger(LOG_INFO, "Initialized radio. Powering radio on.\n");
    if ( !xmpcr_power_on() )
    {
        piboxLogger(LOG_ERROR, "Failed to power on radio.\n");
        return(0);
    }
    piboxLogger(LOG_INFO, "Powered on radio.\n");

    return(1);
}

/*
 *========================================================================
 * Name:   getChoice
 * Prototype:  guint getChoice( void )
 *
 * Description:
 * Retrieves the current DB choice: DB_CATEGORIES or DB_CHANNELS
 *
 * Returns:
 * DB_CATEGORIES if user is browsing categories
 * DB_CHANNELS if user is browsing channels in a category
 *========================================================================
 */
guint
getChoice( void )
{
    return (db_choice);
}

/*
 *========================================================================
 * Name:   loadKeysyms
 * Prototype:  void loadKeysyms( void )
 *
 * Description:
 * Read in the keysym file so we know how the platform wants us to behave.
 *
 * Notes:
 * Format is KEYSYM NAME:ACTION
 *========================================================================
 */
void
loadKeysyms( void )
{
    char        *tsave = NULL;
    struct stat stat_buf;
    char        *keysym;
    char        *action;
    char        *ptr;
    FILE        *fd;
    char        buf[128];
    char        *path;

    if ( isCLIFlagSet( CLI_TEST) )
        path = KEYSYMS_FD;
    else
        path = KEYSYMS_F;

    /* Read in /etc/pibox-keysysm */
    if ( stat(path, &stat_buf) != 0 )
    {
        piboxLogger(LOG_INFO, "No keysym file: %s\n", path);
        return;
    }

    fd = fopen(path, "r");
    if ( fd == NULL )
    {
        piboxLogger(LOG_ERROR, "Failed to open keysyms file: %s - %s\n", path, strerror(errno));
        return;
    }

    memset(buf, 0, 128);
    while( fgets(buf, 127, fd) != NULL )
    {
        /* Ignore comments */
        if ( buf[0] == '#' )
            continue;

        /* Strip leading white space */
        ptr = buf;
        ptr = piboxTrim(ptr);

        /* Ignore blank lines */
        if ( strlen(ptr) == 0 )
            continue;

        /* Strip newline */
        piboxStripNewline(ptr);

        /* Grab first token */
        keysym = strtok_r(ptr, ":", &tsave);
        if ( keysym == NULL )
            continue;

        /* Grab second token */
        action = strtok_r(NULL, ":", &tsave);
        if ( action == NULL )
            continue;

        piboxLogger(LOG_INFO, "keysym / action: %s / %s \n", keysym, action);

        /* Set the home key */
        if ( strncasecmp("home", action, 4) == 0 )
        {
            homeKey = gdk_keyval_from_name(keysym);
            piboxLogger(LOG_INFO, "homeKey = %08x\n", homeKey);
        }
    }
}

/*
 *========================================================================
 * Name:   key_press
 * Prototype:  void key_press( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Handles exiting the application via keystrokes.
 *========================================================================
 */
static gboolean
key_press(GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
    GtkTreePath         *tree_path;
    GtkTreeSelection    *tree_selection;

    switch(event->keyval)
    {
        case GDK_KEY_M:
        case GDK_KEY_m:
            piboxLogger(LOG_INFO, "Mute key\n");
            xmpcr_mute();
            do_menu_drawing();
            return(TRUE);
            break;

        case GDK_KEY_Q:
        case GDK_KEY_q:
            if (event->state & GDK_CONTROL_MASK)
            {
                piboxLogger(LOG_INFO, "Ctrl-Q key\n");
                gtk_main_quit();
                return(TRUE);
            }
            break;

        case GDK_KEY_Escape:
        case GDK_KEY_Home:
            piboxLogger(LOG_INFO, "Home key\n");
            gtk_main_quit();
            return(TRUE);
            break;

        case GDK_KEY_Tab:
            piboxLogger(LOG_INFO, "Tab key\n");
            db_choice = !db_choice;
            populateList(channel_cat_list_store);
            do_menu_drawing();
            db_clear_category();
            tree_path = gtk_tree_path_new_from_indices(0,-1);
            tree_selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(view));
            gtk_tree_selection_select_path(tree_selection, tree_path);
            gtk_tree_path_free(tree_path);
            return(TRUE);
            break;

        case GDK_KEY_space:
        case GDK_KEY_Return:
        case GDK_KEY_KP_Space:
        case GDK_KEY_KP_Enter:
            piboxLogger(LOG_INFO, "Select key\n");
            entry_selected ();
            return(TRUE);
            break;

        default:
            if ( event->keyval == homeKey )
            {
                piboxLogger(LOG_INFO, "Keysym configured home key\n");
                gtk_main_quit();
                return(TRUE);
            }
            else
            {
                piboxLogger(LOG_INFO, "Unknown keysym: %s\n", gdk_keyval_name(event->keyval));
                return(FALSE);
            }
            break;
    }
    return(FALSE);
}

/*
 *========================================================================
 * Name:   update_channel
 * Prototype:  void update_channel( int channel_num )
 *
 * Description:
 * Updates the channel information with info from the specified channel.
 *
 * Notes:
 * Mutex is used here because of possibly async updates.
 *========================================================================
 */
void
update_channel(int channel_num)
{
    char  buf[128];
    int   i;

    pthread_mutex_lock( &channelDisplayMutex );

    gtk_entry_set_text(GTK_ENTRY(station[0]), "");
    gtk_entry_set_text(GTK_ENTRY(station[1]), "");
    gtk_entry_set_text(GTK_ENTRY(station[2]), "");
    gtk_entry_set_text(GTK_ENTRY(station[3]), "");

    sprintf(buf, "%d - %s", (int)channel_list[channel_num].number, channel_list[channel_num].name);
    gtk_entry_set_text(GTK_ENTRY(station[0]), buf);
    gtk_entry_set_text(GTK_ENTRY(station[1]), channel_list[channel_num].category);
    gtk_entry_set_text(GTK_ENTRY(station[2]), channel_list[channel_num].artist);
    gtk_entry_set_text(GTK_ENTRY(station[3]), channel_list[channel_num].title);

    /* Queue widget updates in case this was called outside of UI context. */
    for (i=0; i<4; i++)
        gtk_widget_queue_draw(GTK_WIDGET(station[i]));

    pthread_mutex_unlock( &channelDisplayMutex );
}

/*
 *========================================================================
 * Name:   update_list
 * Prototype:  void update_list( void )
 *
 * Description:
 * Updates the list of channels or categories.
 *
 * Notes:
 * This is a public function to avoid exposing the widget.
 *========================================================================
 */
void
update_list( void )
{
    populateList(channel_cat_list_store);
}

/*
 *========================================================================
 * Name:   entry_selected
 * Prototype:  void entry_selected( GtkTreeSelection *, gpointer )
 *
 * Description:
 * Either show channels associated with the category selected or change
 * to the selected channel.
 *========================================================================
 */
void
entry_selected (void)
{
    GtkTreeIter     iter;
    GtkTreeModel    *model;
    gchar           *entry_text;
    int             i;
    int             channel;

    GtkTreePath         *tree_path;
    GtkTreeSelection    *tree_selection;
    GtkTreeSelection    *selection;

    selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(view));
    if ( db_choice == DB_CATEGORIES )
    {
        piboxLogger(LOG_INFO, "db_choice == DB_CATEGORIES\n");
        if (gtk_tree_selection_get_selected (selection, &model, &iter))
        {
            /* Get category name from selection. */
            gtk_tree_model_get (model, &iter, 0, &entry_text, -1);

            /* Set the current category */
            piboxLogger(LOG_INFO, "Setting category to: %s\n", entry_text);
            db_set_category(entry_text);
        }

        /* Now update the list with channels from that category. */
        // populateList(channel_cat_list_store);
        db_choice = !db_choice;
        populateList(channel_cat_list_store);
        do_menu_drawing();
        db_clear_category();
        tree_path = gtk_tree_path_new_from_indices(0,-1);
        tree_selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(view));
        gtk_tree_selection_select_path(tree_selection, tree_path);
        gtk_tree_path_free(tree_path);
    }
    else
    {
        piboxLogger(LOG_INFO, "db_choice == DB_CHANNELS\n");
        if (gtk_tree_selection_get_selected (selection, &model, &iter))
        {
            /* Clear the current category */
            db_clear_category();

            /* Get category name from selection. */
            gtk_tree_model_get (model, &iter, 0, &entry_text, -1);

            piboxLogger(LOG_INFO, "Looking for channel: *%s*\n", (char *)(entry_text+6));

            /* Update the content area with the channel data */
            for(i=1; i<MAX_CHANNELS; i++)
            {
                piboxLogger(LOG_INFO, "Test channel number[%d]: ch %d, name %s\n", 
                        i, channel_list[i].number,
                        channel_list[i].name
                        );
                if ( (channel_list[i].name != NULL) && 
                     ( strlen(channel_list[i].name) > 0 ) &&
                     ( strncmp(channel_list[i].name, (char *)(entry_text+6), 
                               strlen(channel_list[i].name)) == 0 ) )
                {
                    channel = (int)channel_list[i].number;
                    piboxLogger(LOG_INFO, "Channel number[%d]: %d\n", i, channel);
                    xmpcr_get_channel_info( channel, 1);
                    update_channel( channel );

                    piboxLogger(LOG_INFO, "Selected Channel: \n");
                    piboxLogger(LOG_INFO, "Wanted  : %s\n", entry_text);
                    piboxLogger(LOG_INFO, "channel : %d\n", channel);
                    piboxLogger(LOG_INFO, "name    : %s\n", channel_list[i].name);
                    piboxLogger(LOG_INFO, "category: %s\n", channel_list[i].category);
                    piboxLogger(LOG_INFO, "artist  : %s\n", channel_list[i].artist);
                    piboxLogger(LOG_INFO, "title   : %s\n", channel_list[i].title);

                    /* Change to the new channel */
                    xmpcr_set_channel( channel );

                    break;
                }
            }
        }
    }
}

/*
 *========================================================================
 * Name:   do_menu_drawing
 * Prototype:  void do_menu_drawing( void )
 *
 * Description:
 * Updates the menu area of the display.
 *
 * Notes:
 * See http://zetcode.com/gfx/cairo/cairotext/
 *========================================================================
 */
void
do_menu_drawing()
{
    cairo_t         *menucr = NULL;
    cairo_surface_t *image= NULL;
    GtkRequisition  req;

    if ( pthread_mutex_trylock( &menuDisplayMutex ) != 0 )
        return;

    menucr = gdk_cairo_create(menuArea->window);
    req.width = gdk_window_get_width(menuArea->window);
    req.height = gdk_window_get_height(menuArea->window);
    piboxLogger(LOG_INFO, "window w/h: %d / %d\n", req.width, req.height);

    // Fill with black background
    cairo_set_source_rgb(menucr, 0.0, 0.0, 0.0);
    cairo_set_line_width(menucr, 1);
    cairo_rectangle(menucr, 0, 0, req.width, req.height);
    cairo_stroke_preserve(menucr);
    cairo_fill(menucr);

    /*
     * Draw mute icon
     */
    if ( mute )
    {
        if ( mute_off_image == NULL )
        {
            piboxLogger(LOG_INFO, "Loading mute off icon: %s\n", cliOptions.muteOff);
            mute_off_image = cairo_image_surface_create_from_png(cliOptions.muteOff);
        }
        image = mute_off_image;
    }
    else
    {
        if ( mute_on_image == NULL )
        {
            piboxLogger(LOG_INFO, "Loading mute on icon: %s\n", cliOptions.muteOn);
            mute_on_image = cairo_image_surface_create_from_png(cliOptions.muteOn);
        }
        image = mute_on_image;
    }
    cairo_set_source_surface(menucr, image, (req.width-50), 10);
    cairo_paint(menucr);

    /*
     * Draw device available icon
     */
    if ( radio_init )
    {
        if ( radio_on_image == NULL )
        {
            piboxLogger(LOG_INFO, "Loading radio on icon: %s\n", cliOptions.radioOn);
            radio_on_image = cairo_image_surface_create_from_png(cliOptions.radioOn);
        }
        image = radio_on_image;
    }
    else
    {
        if ( radio_off_image == NULL )
        {
            piboxLogger(LOG_INFO, "Loading radio off icon: %s\n", cliOptions.radioOff);
            radio_off_image = cairo_image_surface_create_from_png(cliOptions.radioOff);
        }
        image = radio_off_image;
    }
    cairo_set_source_surface(menucr, image, (req.width-100), 10);
    cairo_paint(menucr);

    cairo_select_font_face(menucr, "Purisa",
        CAIRO_FONT_SLANT_NORMAL,
        CAIRO_FONT_WEIGHT_BOLD);

    if ( !db_choice )
    {
        cairo_set_font_size(menucr, 15);
        cairo_set_source_rgb(menucr, 1.0, 1.0, 1.0);
    }
    else
    {
        cairo_set_font_size(menucr, 13);
        cairo_set_source_rgb(menucr, 0.75, 0.75, 0.75);
    }
    piboxLogger(LOG_INFO, "Drawing %s\n", CATEGORIES_S);
    cairo_move_to(menucr, 20, 25);
    cairo_show_text(menucr, CATEGORIES_S);

    if ( db_choice )
    {
        cairo_set_font_size(menucr, 15);
        cairo_set_source_rgb(menucr, 1.0, 1.0, 1.0);
    }
    else
    {
        cairo_set_font_size(menucr, 13);
        cairo_set_source_rgb(menucr, 0.75, 0.75, 0.75);
    }
    piboxLogger(LOG_INFO, "Drawing %s\n", CHANNEL_S);
    cairo_move_to(menucr, 220, 25);
    cairo_show_text(menucr, CHANNEL_S);
    cairo_destroy(menucr);
    pthread_mutex_unlock( &menuDisplayMutex );
}

/*
 *========================================================================
 * Name:   on_menudraw_event
 * Prototype:  void on_menudraw_event( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Updates the menu area of the display.
 *========================================================================
 */
static gboolean
on_menudraw_event(GtkWidget *widget, GdkEventExpose *event, gpointer user_data)
{
    piboxLogger(LOG_INFO, "Menu draw event\n");
    do_menu_drawing();
    return TRUE;
}

/*
 *========================================================================
 * Name:   createWindow
 * Prototype:  GtkWidget *createWindow( void )
 *
 * Description:
 * Creates the main window with a list of categories/channels on the left and currently
 * selected channel on the right.  
 *========================================================================
 */
GtkWidget *
createWindow()
{
    int                 i;
    GtkWidget           *vbox;
    GtkWidget           *hbox1;
    GtkCellRenderer     *renderer;
    GtkTreeSelection    *select;
    GtkWidget           *sw;
    GtkWidget           *table;
    GtkWidget           *label;

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    GTK_WIDGET_SET_FLAGS(window, GTK_CAN_FOCUS );
    gtk_widget_add_events(window, GDK_KEY_PRESS_MASK);
    g_signal_connect(G_OBJECT(window),
                "key_press_event",
                G_CALLBACK(key_press),
                NULL);

    /* Two rows: menu and content */
    vbox = gtk_vbox_new (FALSE, 0);
    gtk_widget_set_name (vbox, "vbox");
    gtk_widget_show (vbox);
    gtk_container_add (GTK_CONTAINER (window), vbox);

    /* Menu area */
    menuArea = gtk_drawing_area_new();
    gtk_widget_set_size_request( menuArea, 800, 40);
    gtk_box_pack_start (GTK_BOX (vbox), menuArea, FALSE, FALSE, 0);

    gtk_widget_add_events(menuArea, GDK_BUTTON_PRESS_MASK);
    g_signal_connect(G_OBJECT(menuArea), "expose_event", G_CALLBACK(on_menudraw_event), NULL);

    /* Two columns: list and poster */
    hbox1 = gtk_hbox_new (TRUE, 10);
    gtk_widget_set_name (hbox1, "hbox1");
    gtk_widget_show (hbox1);
    gtk_box_pack_start (GTK_BOX (vbox), hbox1, TRUE, TRUE, 0);

    /* Populate a list model */
    channel_cat_list_store = gtk_list_store_new (1, G_TYPE_STRING);
    populateList(channel_cat_list_store);

    /*
     * Create a scrolled window for the tree view
     */
    sw = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
    gtk_widget_show (sw);
    gtk_box_pack_start (GTK_BOX (hbox1), sw, TRUE, TRUE, 0);

    /*
     * Create the view of the channel/category list
     */
    view = gtk_tree_view_new();
    gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(view), FALSE);
    renderer = gtk_cell_renderer_text_new();
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (view),
                                                 -1,
                                                 NULL,
                                                 renderer,
                                                 "text", 0,
                                                 NULL);

    gtk_tree_view_set_model( GTK_TREE_VIEW(view), GTK_TREE_MODEL(channel_cat_list_store) );
    g_object_unref(G_OBJECT(channel_cat_list_store));
    gtk_widget_show (view);
    gtk_container_add (GTK_CONTAINER (sw), view);

    gtk_tree_view_set_search_equal_func (
            GTK_TREE_VIEW(view), 
            search_func, 
            NULL, 
            NULL);

    /*
     * The content area
     */
    table = gtk_table_new(4, 2, FALSE);
    gtk_box_pack_start (GTK_BOX (hbox1), table, TRUE, TRUE, 10);
    for (i=0; i < 4; i++) 
    {
        label = gtk_label_new(station_labels[i]);
        gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
        station[i] = gtk_entry_new();
        gtk_table_attach(GTK_TABLE(table), label, 
                0, 1, i, i+1,
                GTK_SHRINK,
                0,
                10, 0
                );
        gtk_table_attach(GTK_TABLE(table), station[i], 
                1, 2, i, i+1,
                GTK_EXPAND | GTK_FILL,
                GTK_EXPAND | GTK_FILL,
                0, 0
                );
    }

    /*
     * Setup for selecting from the view.
     */
    select = gtk_tree_view_get_selection (GTK_TREE_VIEW (view));
    gtk_tree_selection_set_mode (select, GTK_SELECTION_SINGLE);

    // Make the main window die when destroy is issued.
    g_signal_connect(window, "destroy", G_CALLBACK (gtk_main_quit), NULL);

    // Now position the window and set its title
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(window), 800, 600);
    gtk_window_set_title(GTK_WINDOW(window), "pixm");

    return window;
}

/*
 * ========================================================================
 * Name:   main
 *
 * Description:
 * Program startup
 *
 * Notes:
 * This is a gtk program in order to properly capture keyboard input events
 * that stop the playback from the webcam.
 * ========================================================================
 */
int
main(int argc, char *argv[])
{
    char    gtkrc[1024];
    char    cwd[512];

    /* Load saved configuration and parse command line */
    initConfig();
    parseArgs(argc, argv);
    validateConfig();

    /* Setup logging */
    piboxLoggerInit(cliOptions.logFile);
    piboxLoggerVerbosity(cliOptions.verbose);
    printf("Verbosity level: %d\n", piboxLoggerGetVerbosity());

    if ( cliOptions.logFile != NULL )
    {
        piboxLogger(LOG_INFO, "Log file: %s\n", cliOptions.logFile);
    }
    else
    {
        piboxLogger(LOG_INFO, "No log file configured.\n");
    }

    /* Read environment config for keyboard behaviour */
    loadKeysyms();

    /*
     * Initialize the radio.
     */
    if ( !initRadio() )
    {
        piboxLogger(LOG_INFO, "Radio is not initialized.\n");
        radio_init = 0;
    }
    else
    {
        piboxLogger(LOG_INFO, "Radio is initialized.\n");
        radio_init = 1;
    }

    gtk_init(&argc, &argv);
    if ( isCLIFlagSet( CLI_TEST) )
    {
        memset(cwd, 0, 512);
        getcwd(cwd, 512);
        sprintf(gtkrc, "%s/data/gtkrc", cwd);
        piboxLogger(LOG_INFO, "gtkrc: %s\n", gtkrc);
        gtk_rc_parse(gtkrc);
    }
    else
        piboxLogger(LOG_INFO, "CLI_TEST is not set.\n");

    window = createWindow();
    gtk_window_fullscreen (GTK_WINDOW(window));
    gtk_widget_show_all(window);

    /* Start the thread that handles grabbing updates from the radio. */
    startChannelMgr();

    piboxLogger(LOG_INFO, "Calling gtk_main\n");
    gtk_main();

    /* Stop channel manager thread. */
    shutdownChannelMgr();

    if ( radio_init )
    {
        xmpcr_power_off();
    }
    piboxLoggerShutdown();
    return 0;
}

