/*******************************************************************************
 * pixm
 *
 * channelMgr.c:  Manage updates to channels
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define CHANNELMGR_C

/* To avoid deprecation warnings from GTK+ 2.x */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <glib.h>
#include <pthread.h>
#include <semaphore.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

static int              mgrIsRunning = 0;
static int              mgrThreadIsRunning = 0;
static pthread_mutex_t  mgrProcessorMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_t        mgrProcessorThread;

#include "pixm.h"

/*
 ******************************************************************************
 ******************************************************************************
 *
 * Thread functions
 *
 ******************************************************************************
 ******************************************************************************
 */

/*========================================================================
 * Name:   isMgrProcessorRunning
 * Prototype:  int isMgrProcessorRunning( void )
 *
 * Description:
 * Thread-safe read of mgrIsRunning variable.
 *========================================================================*/
static int
isMgrProcessorRunning( void )
{
    int status;
    pthread_mutex_lock( &mgrProcessorMutex );
    status = mgrIsRunning;
    pthread_mutex_unlock( &mgrProcessorMutex );
    return status;
}

/*========================================================================
 * Name:   isMgrTheadProcessorRunning
 * Prototype:  int isMgrTheadProcessorRunning( void )
 *
 * Description:
 * Thread-safe read of mgrIsRunning variable.
 *========================================================================*/
int
isMgrThreadRunning( void )
{
    int status;
    pthread_mutex_lock( &mgrProcessorMutex );
    status = mgrThreadIsRunning;
    pthread_mutex_unlock( &mgrProcessorMutex );
    return status;
}

/*========================================================================
 * Name:   setMgrProcessorRunning
 * Prototype:  int setMgrProcessorRunning( void )
 *
 * Description:
 * Thread-safe set of mgrIsRunning variable.
 *========================================================================*/
static void
setMgrProcessorRunning( int val )
{
    pthread_mutex_lock( &mgrProcessorMutex );
    mgrIsRunning = val;
    pthread_mutex_unlock( &mgrProcessorMutex );
}

/*========================================================================
 * Name:   setMgrThreadRunning
 * Prototype:  int setMgrThreadRunning( void )
 *
 * Description:
 * Thread-safe set of mgrTheadIsRunning variable.
 *========================================================================*/
static void
setMgrThreadRunning( int val )
{
    pthread_mutex_lock( &mgrProcessorMutex );
    mgrThreadIsRunning = val;
    pthread_mutex_unlock( &mgrProcessorMutex );
}

/*========================================================================
 * Name:   mgrProcessorTimer
 * Prototype:  void mgrProcessorTime( gpointer data )
 *
 * Description:
 * Grab channel and signal info from the radio.
 *
 * Input arguments:
 * data     Required by g_timeout_add API but is not used in this function.
 *
 * Returns:
 * TRUE, always, which allows the timer to keep running.
 *
 * Notes:
 * This loop runs from the timeout set in pixm.c. The timeout function isn't
 * rescheduled until this function returns TRUE so there is no risk of this
 * function being called asynchronously multiple times.
 *========================================================================*/
gboolean 
mgrProcessorTimer( gpointer data )
{
    static int              channel = 1;    /* Next channel to scan when iterating all channels. */
    static unsigned char    mode = 1;       /* 1: current channel, 2: signal info, 3: next channel scan. */
    static unsigned char    iter = 1;       /* (iter==4)?mode=1 */
    int                     active_channel;

    switch(mode)
    {
        case 1:
            /* Get current channel info. */
            piboxLogger(LOG_TRACE2, "Fetching current channel info.\n");
            active_channel = xmpcr_get_active_channel();
            if ( active_channel != -1 )
            {
                xmpcr_get_channel_info(active_channel, 0);
                update_channel( active_channel );
            }

            /* Get signal info on next call. */
            mode=2;
            break;

        case 2:
            /* Get signal info. */
            piboxLogger(LOG_TRACE2, "Fetching signal info.\n");
            xmpcr_get_signal_info();

            /* Reset to channel scan on next call. */
            mode=3;
            break;

        /* Get next channel info. */
        default:
            piboxLogger(LOG_TRACE2, "Fetching channel %d info.\n", channel);

            xmpcr_get_channel_info( channel, 1 );
            channel++;

            /* Cycle back when we reach the end of channels */
            if ( channel == (MAX_CHANNELS+1) )
            {
                piboxLogger(LOG_TRACE2,"All channels retrieved\n");
                channel=1;
            }

            /* Restart series after given iterations. */
            iter++;
            if ( iter == 5 )
            {
                mode = 1;
                iter = 1;
            }
            break;
    }

    return TRUE;
}

/*========================================================================
 * Name:   mgrProcessor
 * Prototype:  void mgrProcessor( void * )
 *
 * Description:
 * Grab channel data from the radio.
 *
 * Input Arguments:
 * void *arg    Not used, but required for threads.
 *
 * Notes:
 * We use a secondary thread to schedule a timer so that the timer won't
 * interfere with the UI thread.
 *========================================================================*/
static void *
mgrProcessor( void *arg )
{
    setMgrProcessorRunning(1);
    setMgrThreadRunning(1);

    piboxLogger(LOG_INFO, "Getting radio ID.\n");
    while ( isMgrProcessorRunning() )
    {
        xmpcr_get_radio_id();
        if ( strlen(xmpcr.radio_id) > 0 )
        {
            piboxLogger(LOG_INFO, "Radio ID: %s\n", xmpcr.radio_id);
            break;
        }
    }

    piboxLogger(LOG_INFO, "Getting radio version info.\n");
    while ( isMgrProcessorRunning() )
    {
        xmpcr_get_info();
        if ( strlen(xmpcr.sdec_version) > 0 )
        {
            piboxLogger(LOG_INFO, "SDEC    : %s\n", xmpcr.sdec_version);
            piboxLogger(LOG_INFO, "XMSTK   : %s\n", xmpcr.xmstk_version);
            break;
        }
    }

    // This doesn't appear to be working.
    // xmpcr_get_signal_info();
    // piboxLogger(LOG_INFO, "Got signal info.\n");

    /* Spin till we're notified to exit. */
    while ( isMgrProcessorRunning() )
    {
        mgrProcessorTimer((gpointer)NULL);
    }

    piboxLogger(LOG_INFO, "Mgr thread is exiting.\n");
    setMgrThreadRunning(0);
    return(0);
}

/*========================================================================
 * Name:   startChannelMgr
 * Prototype:  void startChannelMgr( void )
 *
 * Description:
 * Setup thread to handle inbound messages.
 *
 * Notes:
 * Threads run until configs->serverEnabled = 0.  See shutdownMgrProcessor().
 *========================================================================*/
void
startChannelMgr( void )
{
    int rc;

    // Prevent running two at once.
    if ( isMgrProcessorRunning() )
        return;

    /* Create a thread to expire streams. */
    rc = pthread_create(&mgrProcessorThread, NULL, &mgrProcessor, (void *)NULL);
    if (rc)
    {
        piboxLogger(LOG_ERROR, "%s: Failed to create mgr thread: %s\n", PROG, strerror(rc));
        exit(-1);
    }
    piboxLogger(LOG_INFO, "%s: Started mgr thread.\n", PROG);
    return;
}

/*========================================================================
 * Name:   shutdownMgrProcessor
 * Prototype:  void shutdownMgrProcessor( void )
 *
 * Description:
 * Shut down message processing thread.
 *========================================================================*/
void
shutdownChannelMgr( void )
{
    int timeOut = 0;

    setMgrProcessorRunning(0);
    if ( isMgrThreadRunning() )
    {
        while ( isMgrThreadRunning() )
        {
            sleep(1);
            timeOut++;
            if (timeOut == 5)
            {
                piboxLogger(LOG_ERROR, "Timed out waiting on mgr thread to shut down.\n");
                pthread_cancel(mgrProcessorThread);
                piboxLogger(LOG_INFO, "mgrProcessor forced shut down.\n");
                return;
            }
        }
        pthread_detach(mgrProcessorThread);
    }
    piboxLogger(LOG_INFO, "mgrProcessor shut down.\n");
}

