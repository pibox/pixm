/*******************************************************************************
 * pixm
 *
 * db.c:  Functions for managing categories and channels
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define DB_C

/* To avoid deprecation warnings from GTK+ 2.x */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <glib.h>
#include <pibox/utils.h>
#include <pibox/parson.h>

#include "pixm.h"

/* Currently selected category, if any. */
static char *active_category = NULL;

static pthread_mutex_t listDisplayMutex = PTHREAD_MUTEX_INITIALIZER;

/*
 *========================================================================
 * Name:   db_set_category
 * Prototype:  void db_set_category( char *category )
 *
 * Description:
 * Set the active category.
 *========================================================================
 */
void
db_set_category( char *category )
{
    if ( category == NULL )
        return;

    if ( active_category != NULL )
        free(active_category);
    active_category = strdup(category);
}

/*
 *========================================================================
 * Name:   db_clear_category
 * Prototype:  void db_clear_category( char *category )
 *
 * Description:
 * Clear the active category.
 *========================================================================
 */
void
db_clear_category( void )
{
    if ( active_category != NULL )
        free(active_category);
    active_category = NULL;
}

/*
 *========================================================================
 * Name:   populateList
 * Prototype:  void populateList( GtkWidget * )
 *
 * Description:
 * Fills list with the names of categories or channels.
 *========================================================================
 */
void
populateList (GtkListStore *listStore)
{
    GtkTreeIter         iter;
    int                 j;
    char                buf[64];

    pthread_mutex_lock( &listDisplayMutex );
    gtk_list_store_clear (listStore);
    if ( getChoice() == DB_CATEGORIES )
    {
        for(j = 0; j < MAX_CATEGORIES; j++) 
        {
            if ( category_list[j] == NULL )
                continue;

            if ( strlen(category_list[j]) > 0 )
            {
                piboxLogger(LOG_INFO, "Adding category: %s\n", category_list[j]);
                gtk_list_store_append (listStore, &iter);
                gtk_list_store_set(listStore, &iter, 0, category_list[j], -1);
            }
        }
    }
    else
    {
        for(j = 0; j < MAX_CHANNELS; j++) 
        {
            if ( (channel_list[j].name != NULL) && (strlen(channel_list[j].name)>0) )
            {
                if ((active_category!=NULL) && (strcmp(channel_list[j].category,active_category)!=0))
                    continue;

                sprintf(buf, "%3d - %s", (int)channel_list[j].number, channel_list[j].name);
                gtk_list_store_append (listStore, &iter);
                gtk_list_store_set(listStore, &iter, 0, buf, -1);
            }
        }
    }

    /* Schedule an update of the view widget which contains the listStore. */
    pthread_mutex_unlock( &listDisplayMutex );
}
