/*******************************************************************************
 * pixm
 *
 * channelMgr.h:  Manage updates to channels
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef CHANNELMGR_H
#define CHANNELMGR_H

/*========================================================================
 * Defined values
 *=======================================================================*/

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/


/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef CHANNELMGR_C
extern gboolean mgrProcessor( gpointer data );
extern void startChannelMgr( void );
extern void shutdownChannelMgr( void );
#endif /* !CHANNELMGR_C */
#endif /* !CHANNELMGR_H */
