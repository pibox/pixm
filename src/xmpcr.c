/*******************************************************************************
 * pixm
 *
 * xmpcr.c:  XMPCR communications module
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 *
 * Based in part on
 * mmxmpcr: http://www.michaelminn.com/linux/mmxmpcr/
 * OpenXM (no longer available)
 ******************************************************************************/
#define XMPCR_C

/* To avoid deprecation warnings from GTK+ 2.x */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <sys/poll.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <termios.h>

#include "pixm.h"

#define XMPCR_BUFFER_LENGTH 512
#define MAX_RETRIES         10

/*
 * Handle on open device port.
 */
static int  xmpcr_fd;

/*
 * Currently active channel.
 */
static int active_channel = -1;

/*
 * Global configuration and response structure.
 */
XMPCR_T xmpcr;

/*
 * Signal information
 */
SIGNALINFO_T    signalInfo;

/*
 * Thread mutex.
 */
pthread_mutex_t channelMutex = PTHREAD_MUTEX_INITIALIZER;

/*
 * Keep track of channels
 */
CHANINFO_T channel_list[MAX_CHANNELS];
char *category_list[MAX_CATEGORIES];

/*
 * Mute state
 */
char mute = 1;

/*
 * Local prototypes.
 */
void xmpcr_reset( void );
int xmpcr_init( int clear );

/*
 * ========================================================================
 * ========================================================================
 * Static functions
 * ========================================================================
 * ========================================================================
 */

/*========================================================================
 * Name:   dump_message
 * Prototype:  int dump_message( char *title, char *message, int length )
 *
 * Description:
 * Print out the message received from the device.
 * 
 * Returns:
 * 0 if write fails.
 * 1 if write succeeds.
 *========================================================================*/
static int 
dump_message(char *title, unsigned char *message, int length)
{
    int     scan;
    char    buf[1024];
    char    *ptr = buf;

    memset(buf, 0, 1024);
    // printf("---------------- %s -----------------\n", title);
    // piboxLogger(LOG_INFO, "---------------- %s -----------------\n", title);

    for (scan = 0; scan < length; ++scan)
    {
        sprintf(ptr, "%02x ", message[scan]);
        ptr += 3;

        if ((scan & 0xf) == 0xf)
        {
            sprintf(ptr, " ");
            ptr++;
        }
    }

    sprintf(ptr, "| ");
    ptr++;
    ptr++;

    for (scan = 0; scan < length; ++scan)
    {
        if ((message[scan] < 32) || (message[scan] > 127))
        {
            sprintf(ptr, "-");
            ptr++;
        }
        else
        {
            sprintf(ptr, "%c", message[scan]);
            ptr++;
        }

        if ((scan & 0xf) == 0xf)
        {
            sprintf(ptr, " ");
            ptr++;
        }
    }

    // sprintf(ptr, "\n\n");
    sprintf(ptr, "\n");
    piboxLogger(LOG_INFO, "%s", buf);
    return length;
}

/*========================================================================
 * Name:    addCategory
 * Prototype:  void addCategory( char *title, char *message, int length )
 *
 * Description:
 * Add a category to the list of categories, if it's not already there.
 *========================================================================*/
static void 
addCategory(char *entry )
{
    char    found = 0;
    int     i;

    for(i=0; i<MAX_CATEGORIES-1; i++)
    {
        /* If we find a NULL entry we're at the end of the list. */
        if ( category_list[i] == NULL )
            break;

        /* Is the current list entry a match?  If so, we already have this category. */
        if ( strcmp(category_list[i],entry) == 0 )
        {
            found = 1;
            break;
        }
    }
    if ( !found )
    {
        /* We didn't find the category in the current list so add it here. */
        piboxLogger(LOG_INFO, "Adding category[%d]: %s\n", i, entry);
        category_list[i] = strdup(entry);
    }
}

/*========================================================================
 * Name:   port_write
 * Prototype:  int port_write( unsigned char *bytes, int length )
 *
 * Description:
 * Write the specified bytes to the port.
 * 
 * Returns:
 * 0 if write fails.
 * 1 if write succeeds.
 *========================================================================*/
static int
port_write( unsigned char *bytes, int length )
{
    int rc = 1;

    if ( write (xmpcr_fd, bytes, length) < 0 )
    {
        piboxLogger(LOG_ERROR, "Failed write to port.\n");
        rc=0;
    }
    return(rc);
}

/*========================================================================
 * Name:   parse_response
 * Prototype:  int parse_response( unsigned char *buf, unsigned short length, unsigned char expected )
 *
 * Description:
 * Parse a response from the radio.  All responses update the global
 * xmpcr structure.
 * 
 * Returns:
 * 0 if parse fails.
 * 1 if parse succeeds.
 *========================================================================*/
int 
parse_response( unsigned char *buf, unsigned short len, unsigned char expected ) 
{
    int     offset = 0;

    if ( buf[0] != expected )
    {
        piboxLogger(LOG_ERROR, "Not expected return.  Skipping.\n");
        return(0);
    }

    switch(buf[0]) 
    {
        /* HELLO response */
        case 0x80:
            sprintf(xmpcr.sdec_version, "%d, %d/%d/%d",
                BCD2DEC(buf[4]),
                BCD2DEC(buf[5]),
                BCD2DEC(buf[6]),
                BCD2DEC(buf[7])*100+BCD2DEC(buf[8])
                );

            sprintf(xmpcr.xmstk_version, "%d, %d/%d/%d",
                BCD2DEC(buf[13]),
                BCD2DEC(buf[14]),
                BCD2DEC(buf[15]),
                BCD2DEC(buf[16])*100+BCD2DEC(buf[17])
                );
            sprintf(xmpcr.radio_id, "%.8s",buf+19);
            break;

        case 0x81: // GOODBYE
            sprintf(xmpcr.goodbye, "%.2x %.2x %.2x",buf[1],buf[2],buf[3]);
            break;

        case 0x93: // ?? response to unk1300
            // sprintf("UNK [0x93]: %.2x %.2x %.2x\n",buf[1],buf[2],buf[3]);
            break;

        case 0xa5: // CHINFO   -- channel number is correct with req 8 but off by one with req 9?

            // dump_message("channel info", buf, len);

            /*
             * Look for the marker that ends the header.
             */
            if ( buf[5] != 0x01 )
            {
                /*
                 * For some reason, sometimes the header is one byte short.
                 */
                if ( buf[4] == 0x01 )
                {
                    offset = 1;
                }
                else
                {
                    /* Invalid buffer - try again! */
                    piboxLogger(LOG_ERROR, "Invalid get-channel buffer. Skipping.\n");
                    return(0);
                }
            }

            memset(&xmpcr, 0, sizeof(xmpcr));

            /* This is the current station info */
            sprintf(xmpcr.chan_num, "%d",    buf[3]);
            sprintf(xmpcr.station,  "%.16s", buf+6-offset);
            sprintf(xmpcr.category, "%.16s", buf+24-offset);
            sprintf(xmpcr.artist,   "%.16s", buf+41-offset);
            sprintf(xmpcr.title,    "%.16s", buf+57-offset);

            piboxLogger(LOG_INFO, "Got channel num: %s\n", xmpcr.chan_num);
            piboxLogger(LOG_INFO, "Station    : %s\n", xmpcr.station);
            piboxLogger(LOG_INFO, "category   : %s\n", xmpcr.category);
            piboxLogger(LOG_INFO, "Artist     : %s\n", xmpcr.artist);
            piboxLogger(LOG_INFO, "Title      : %s\n", xmpcr.title);

            addCategory(xmpcr.category);

            /* Update global channel information */
            channel_list[buf[3]].number = buf[3];
            // memcpy(channel_list[buf[3]].name, buf+6-offset, 16);
            // memcpy(channel_list[buf[3]].category, buf+24-offset, 16);
            // memcpy(channel_list[buf[3]].artist, buf+41-offset, 16);
            // memcpy(channel_list[buf[3]].title, buf+57-offset, 16);

            memcpy(channel_list[buf[3]].name, xmpcr.station, 16);
            memcpy(channel_list[buf[3]].category, xmpcr.category, 16);
            memcpy(channel_list[buf[3]].artist, xmpcr.artist, 16);
            memcpy(channel_list[buf[3]].title, xmpcr.title, 16);
            break;

        case 0xb1: // ID reponse:
            sprintf(xmpcr.radio_id, "%.8s", buf+4);
            break;

        case 0xc1: // status?
            // printf("UNK [0xC1]: status??\n");
            break;

        case 0xc2: // ?? response to unk4201
            // printf("UNK [0xC2]: %.2x %.2x\n",buf[1],buf[2]);
            break;

        case 0xc3: // Signal information
            /*
             * 5AA500 = START COMMAND STRING
             * 0 01 = Command Length
             * 1 43 = TECH COMMAND
             * EDED = END COMMAND STRING 
             * Returns 32 Bytes
             */
            // dump_message("parse_response", buf, len);
            signalInfo.satSignal = buf[3];
            signalInfo.antStatus = buf[4];
            signalInfo.terSignal = buf[5];

            piboxLogger(LOG_INFO, "Satellite signal  : %d\n", signalInfo.satSignal);
            piboxLogger(LOG_INFO, "Antenna status    : %d\n", signalInfo.antStatus);
            piboxLogger(LOG_INFO, "Terristrial signal: %d\n", signalInfo.terSignal);
            break;

        case 0xd0: // status?
            // printf("UNK [0xD0]: %.2x %.2x %.2x\n", buf[1],buf[2],buf[3]);
            break;

        case 0xE0: // Random responses like this come from the Radio every now and then.
            // printf("XM ACK? 0xE0\n");
            piboxLogger(LOG_ERROR, "Got 0xE0.  Skipping.\n");
            return(0);
            break;

        case 0xE3: // XM INFO 2
            piboxLogger(LOG_INFO, "Got 0xE3 (dump_message)\n");
            dump_message("General info (0xE3)", buf, len);
            sprintf(xmpcr.cbm_version, "%d, %d/%d/%d",
                BCD2DEC(buf[4]),
                BCD2DEC(buf[5]),
                BCD2DEC(buf[6]),
                BCD2DEC(buf[7])*100+BCD2DEC(buf[8])
                );

            sprintf(xmpcr.xmstk_version, "%d, %d/%d/%d",
                BCD2DEC(buf[13]),
                BCD2DEC(buf[14]),
                BCD2DEC(buf[15]),
                BCD2DEC(buf[16])*100+BCD2DEC(buf[17])
                );

            sprintf(xmpcr.sdec_version, "%d, %d/%d/%d",
                BCD2DEC(buf[4]),
                BCD2DEC(buf[5]),
                BCD2DEC(buf[6]),
                BCD2DEC(buf[7])*100+BCD2DEC(buf[8])
                );
            break;

        default:
            break;
    }
    piboxLogger(LOG_INFO, "Parse completed successfully.\n");
    return(1);
}

/*========================================================================
 * Name:   get_packet
 * Prototype:  int get_packet( unsigned char *bytes, unsigned int max_length)
 *
 * Description:
 * Read a packet from the device.
 * 
 * Returns:
 * 0 if write fails.
 * 1 if write succeeds.
 *========================================================================*/
int
get_packet(unsigned char *buf, int max_length) 
{
    fd_set          rfds;
    struct timeval  tv;
    unsigned char   *ptr = buf;
    short           length = 0;
    int             remaining = -1;
    int             numread, totalread=0;
    int             rc = 0;
    int             cnt;
    int             mode;
    int             bytes;

    piboxLogger(LOG_INFO, "Entered.\n");

    /* Maximum times we loop before giving up. */
    FD_ZERO(&rfds);
    FD_SET(xmpcr_fd,&rfds);
    cnt=50;
    mode=0;
    while( (remaining!=0) && (cnt>0) )
    {
        tv.tv_sec=0; 
        tv.tv_usec=150000;
        rc = select(xmpcr_fd+1,&rfds,NULL,NULL,&tv);
        switch (rc)
        {
            case -1:
                cnt--;
                piboxLogger(LOG_ERROR, "select error: %s\n", strerror(errno));
                continue;

            case 0:
                cnt--;
                // piboxLogger(LOG_TRACE2, "select timeout\n");
                continue;
        }

        /* ------NEW NEW NEW ------ */
        switch(mode)
        {
            case 0:
                piboxLogger(LOG_TRACE2, "Checking for 0x5A prefix\n");
                bytes = read(xmpcr_fd,buf,1);
                if ( bytes == 0 )
                {
                    cnt--;
                    continue;
                }

                if ( buf[0] != 0x5A ) 
                {
                    piboxLogger(LOG_ERROR, "Expected 0x5A, got 0x%.2x\n", buf[0]);
                    cnt--;
                    continue;
                }
                mode++;
                continue;
                break;

            case 1:
                piboxLogger(LOG_TRACE2, "Checking for next 0xA5 prefix\n");
                bytes = read(xmpcr_fd,buf,1);
                if ( bytes == 0 )
                {
                    cnt--;
                    continue;
                }
                if( buf[0] != 0xA5) 
                {
                    piboxLogger(LOG_ERROR, "Expected 0xA5, got 0x%.2x\n", buf[0]);
                    cnt--;
                    continue;
                }
                mode++;
                continue;
                break;

            case 2:
                piboxLogger(LOG_TRACE2, "Reading response length byte 1\n");
                bytes = read(xmpcr_fd,buf,1);
                if ( bytes == 0 )
                {
                    cnt--;
                    continue;
                }
                mode++;
                continue;
                break;

            case 3:
                piboxLogger(LOG_TRACE2, "Reading response length byte 2\n");
                bytes = read(xmpcr_fd,buf+1,1);
                if ( bytes == 0 )
                {
                    cnt--;
                    continue;
                }

                /* Convert length from big-endian */
                piboxLogger(LOG_TRACE2, "Response: %02x %02x\n", buf[0], buf[1]);
                length = (buf[0]<<8)+buf[1];
                piboxLogger(LOG_TRACE2, "length: %d\n", length);
                if ( length > 225) 
                {
                    piboxLogger(LOG_TRACE2, "Bad length - starting over.\n");
                    mode=0;
                    cnt=10;
                    continue;
                }

                remaining=length+2;
                if ( remaining > max_length )
                {
                    remaining=max_length;
                }
                piboxLogger(LOG_TRACE2, "Remaining: %d\n", remaining);
                mode++;
                cnt=10;
                continue;
                break;

            default:
                numread=read(xmpcr_fd,ptr,remaining);
                if ( numread == 0 )
                {
                    cnt--;
                    continue;
                }
                piboxLogger(LOG_TRACE2, "Read: %d\n", numread);
                totalread += numread;
                piboxLogger(LOG_INFO, "get_packet dump message.\n");
                dump_message("get_packet partial", ptr, numread);
                remaining-=numread;
                ptr+=numread;
                cnt--;
                break;
        }
    }

    piboxLogger(LOG_INFO, "Checking totalread / remaining: %d / %d\n", totalread, remaining);
    if ( totalread > 0 )
    {
        piboxLogger(LOG_INFO, "total_read dump message.\n");
        dump_message("get_packet", buf, totalread);
    }

    /* If the loop expired before getting all the data, then we failed. */
    piboxLogger(LOG_INFO, "Checking cnt: %d\n", cnt);
    if ( (cnt == 0) && (remaining > 2) )
    {
        piboxLogger(LOG_TRACE2, "Loop expired before getting all data.\n");
        length=-1;
    }

    piboxLogger(LOG_INFO, "Returning length: %d\n", length);
    return length;
}

/*========================================================================
 * Name:   get_response
 * Prototype:  int get_response( unsigned char expected )
 *
 * Description:
 * Read a response from the device.
 * 
 * Returns:
 * non-zero if read fails.
 * 0 if read succeeds.
 *========================================================================*/
static int 
get_response( unsigned char expected ) 
{
    unsigned char buf[1024];
    unsigned int cnt=0;
    int length;

    while (cnt<MAX_RETRIES)
    {
        cnt++;
        memset(buf, 0, 1024);
        piboxLogger(LOG_INFO, "Calling get_packet()\n");
        length=get_packet(buf, 1024);
        if ( length <= 0 )
        {
            cnt++;
            continue;
        }
        piboxLogger(LOG_INFO, "get_response dump message.\n");
        dump_message("get_response", buf, length);
        if ( expected != 0x00 )
        {
            piboxLogger(LOG_INFO, "Calling parse_response()\n");
            if ( parse_response(buf,(unsigned short)length,expected) == 0 )
            {
                return(-1);
            }
            else
            {
                break;
            }
        }
        else
        {
            break;
        }
    }
    if ( cnt == MAX_RETRIES )
    {
        piboxLogger(LOG_INFO, "Timed out getting and parsing response.\n");
        return(1);
    }
    return(0);
}

/*
 * ========================================================================
 * ========================================================================
 * Public functions
 * ========================================================================
 * ========================================================================
 */

/*========================================================================
 * Name:   xmpcr_init
 * Prototype:  int xmpcr_init( void )
 *
 * Description:
 * Initialize the XMPCR by opening the device and setting up communications
 * parameters.
 * 
 * Returns:
 * 0 if initialization fails.
 * 1 if initialization succeeds.
 *========================================================================*/
int
xmpcr_init( int clear )
{
    struct termios  options;

    piboxLogger(LOG_INFO, "Opening XM PCR on %s\n", cliOptions.port);

    if ( clear )
    {
        piboxLogger(LOG_INFO, "Clearing buffers.\n");

        /* Clear response buffer */
        memset(&xmpcr, 0, sizeof(xmpcr));

        /* Clear the category list */
        memset(category_list, 0, MAX_CATEGORIES*sizeof(char *));
        memset(channel_list, 0, MAX_CHANNELS*sizeof(CHANINFO_T));
    }

    xmpcr_fd = open(cliOptions.port, O_RDWR | O_NONBLOCK );
    if (xmpcr_fd < 0)
    {
        piboxLogger(LOG_ERROR, "Failed to open device: %s\n", strerror(errno)); 
        return(0);
    }

    piboxLogger(LOG_INFO, "Setting port options.\n");
    memset(&options, 0, sizeof(options));
    if (tcgetattr(xmpcr_fd, &options) < 0)
    {
        piboxLogger(LOG_ERROR, "Failed to get port attributes.\n"); 
        close(xmpcr_fd);
        return(0);
    }

    // Turn off all input processing
    // memset(&options, 0, sizeof(options));

    /* BAUD rate */
    cfsetispeed(&options, B9600);
    cfsetospeed(&options, B9600);

    /* No Parity, 8 data bits, 1 stop bit (8N1) */
    options.c_cflag &= ~PARENB;
    options.c_cflag &= ~CSTOPB;
    options.c_cflag &= ~CSIZE;
    options.c_cflag |= CS8;

    /* Turn on READ & ignore ctrl lines */
    options.c_iflag |= CREAD | CLOCAL;

    options.c_cc[VMIN]   =  1;              // read doesn't block
    options.c_cc[VTIME]  =  5;              // 0.5 seconds read timeout

    /* No hardware flow control */
    // options.c_cflag &= ~CRTSCTS;

    // unbuffered input so there's no wait for an EOL character
    // options.c_lflag &= (~ICANON);
    options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);

    // if (tcsetattr(xmpcr_fd, TCSAFLUSH, &options) < 0)
    if (tcsetattr(xmpcr_fd, TCSANOW, &options) < 0)
    {
        piboxLogger(LOG_ERROR, "Failed to set port attributes.\n"); 
        close(xmpcr_fd);
        return(0);
    }

    piboxLogger(LOG_INFO, "Port initialized.\n");
    return(1);
}

/*========================================================================
 * Name:   xmpcr_power_on
 * Prototype:  int xmpcr_power_on( void )
 *
 * Description:
 * Power on the XMPCR. Wait for response and ack.
 * 
 * Returns:
 * 0 if power on fails.
 * 1 if power on succeeds.
 *========================================================================*/
int
xmpcr_power_on( void )
{
    int             rc = 1;
    unsigned char   power_on[11] = 
                        { 0x5A, 0xA5, 0x00, 0x05, 0x00, 0x10, 0x10, 0x10, 0x01, 0xED, 0xED };

    piboxLogger(LOG_INFO, "Entered.\n");
    pthread_mutex_lock( &channelMutex );

    if ( !xmpcr_init( FALSE ) )
    {
        piboxLogger(LOG_ERROR, "Failed to init port.\n");
        rc=0;
        goto xmpcr_power_on_exit;
    }

    if ( port_write(power_on, 11) )
    {
        usleep(1000);
        get_response(0x80);
    }
    else
    {
        rc=1;
    }
    close(xmpcr_fd);

xmpcr_power_on_exit:
    pthread_mutex_unlock( &channelMutex );
    return(rc);
}

/*========================================================================
 * Name:   xmpcr_power_off
 * Prototype:  int xmpcr_power_off( void )
 *
 * Description:
 * Power off the XMPCR. Wait for response and ack.
 * 
 * Returns:
 * 0 if power off fails.
 * 1 if power off succeeds.
 *========================================================================*/
int
xmpcr_power_off( void )
{
    int             rc = 0;
    unsigned char   power_off[8] = { 0x5A, 0xA5, 0x00, 0x02, 0x01, 0x00, 0xED, 0xED };

    piboxLogger(LOG_INFO, "Entered.\n");
    pthread_mutex_lock( &channelMutex );

    if ( !xmpcr_init( FALSE ) )
    {
        piboxLogger(LOG_ERROR, "Failed to init port.\n");
        goto xmpcr_power_off_exit;
    }

    if ( port_write(power_off, 8) )
    {
        usleep(1000);
        get_response(0x81);
    }
    else
    {
        rc=1;
    }
    close(xmpcr_fd);

xmpcr_power_off_exit:
    pthread_mutex_unlock( &channelMutex );
    return(rc);
}

/*========================================================================
 * Name:   xmpcr_get_radio_id
 * Prototype:  int xmpcr_get_radio_id( void )
 *
 * Description:
 * Power off the XMPCR. Wait for response and ack.
 * 
 * Returns:
 * The radio ID as a string or NULL if the ID can't be read.
 *========================================================================*/
void
xmpcr_get_radio_id( void )
{
    unsigned char   get_radio_id[7] = { 0x5A, 0xA5, 0x00, 0x01, 0x31, 0xED, 0xED };

    piboxLogger(LOG_INFO, "Entered.\n");
    pthread_mutex_lock( &channelMutex );

    if ( !xmpcr_init( FALSE ) )
    {
        piboxLogger(LOG_ERROR, "Failed to init port.\n");
        goto xmpcr_radio_id_exit;
    }

    if ( port_write(get_radio_id, 7) )
    {
        usleep(1000);
        get_response(0xB1);
    }
    close(xmpcr_fd);

xmpcr_radio_id_exit:
    pthread_mutex_unlock( &channelMutex );
    return;
}

/*========================================================================
 * Name:   xmpcr_get_info
 * Prototype:  int xmpcr_get_info( void )
 *
 * Description:
 * Retrieve radio info.
 *========================================================================*/
void
xmpcr_get_info( void )
{
    unsigned char   get_radio_info[8] = { 0x5A, 0xA5, 0x00, 0x02, 0x70, 0x05, 0xED, 0xED };

    piboxLogger(LOG_INFO, "Entered.\n");
    pthread_mutex_lock( &channelMutex );

    if ( !xmpcr_init( FALSE ) )
    {
        piboxLogger(LOG_ERROR, "Failed to init port.\n");
        goto xmpcr_info_exit;
    }

    if ( port_write(get_radio_info, 8) )
    {
        usleep(1000);
        get_response(0xE3);
    }
    close(xmpcr_fd);

xmpcr_info_exit:
    pthread_mutex_unlock( &channelMutex );
    return;
}

/*========================================================================
 * Name:   xmpcr_get_channel_info
 * Prototype:  int xmpcr_get_channel_info( unsigned char number, int dump_channel )
 *
 * Description:
 * Retrieve channel info.
 * 
 * Arguments:
 * unsigned char number         channel number to retrieve.
 * int           dump_channel   !0: dump channel information to log.
 *
 * Returns:
 * 0 on failure
 * 1 on success
 *========================================================================*/
int
xmpcr_get_channel_info( unsigned char number, int dump_channel )
{
    int             rc = 1;
    unsigned char   channel_info_msg[10] = 
                        { 0x5A, 0xA5, 0x00, 0x04, 0x25, 0x08, 0x00, 0x00, 0xED, 0xED };

                        // { 0x5A, 0xA5, 0x00, 0x04, 0x25, 0x09, 0x00, 0x00, 0xED, 0xED };

    if ( pthread_mutex_trylock( &channelMutex ) )
        return 0;

    piboxLogger(LOG_INFO, "Entered\n");

    if ( !xmpcr_init( FALSE ) )
    {
        piboxLogger(LOG_ERROR, "Failed to init port.\n");
        rc=0;
        goto xmpcr_channel_info_exit;
    }

    channel_info_msg[6] = number;
    piboxLogger(LOG_INFO, "port write\n");
    if ( port_write(channel_info_msg, 10) )
    {
        usleep(1000);
        piboxLogger(LOG_INFO, "get response\n");
        if ( !get_response(0xA5) )
            rc=0;
    }
    close(xmpcr_fd);

    if ( dump_channel && (rc==1) )
    {
        piboxLogger(LOG_INFO, "Want channel Num : %d\n", number);
        piboxLogger(LOG_INFO, "Channel Name: %s\n", channel_list[number].name);
        piboxLogger(LOG_INFO, "category    : %s\n", channel_list[number].category);
        piboxLogger(LOG_INFO, "Artist      : %s\n", channel_list[number].artist);
        piboxLogger(LOG_INFO, "Title       : %s\n", channel_list[number].title);
    }

xmpcr_channel_info_exit:
    pthread_mutex_unlock( &channelMutex );
    return(rc);
}

/*========================================================================
 * Name:   xmpcr_get_signal_info
 * Prototype:  int xmpcr_get_signal_info( void )
 *
 * Description:
 * Retrieve radio signal info.  This is the satellite and terrestrial
 * signal data.
 *
 * Notes:
 * This doesn't appear to work, possibly because the data it retrieves is
 * not formatted the same as other responses.
 *========================================================================*/
void
xmpcr_get_signal_info( void )
{
    unsigned char   signal_info[7] = { 0x5A, 0xA5, 0x00, 0x01, 0x43, 0xED, 0xED };

    piboxLogger(LOG_INFO, "Entered.\n");
    pthread_mutex_lock( &channelMutex );

    if ( !xmpcr_init( FALSE ) )
    {
        piboxLogger(LOG_ERROR, "Failed to init port.\n");
        goto xmpcr_signal_info_exit;
    }

    if ( port_write(signal_info, 7) )
    {
        usleep(1000);
        piboxLogger(LOG_INFO, "Calling get_response(0xC3)\n");
        get_response(0xC3);
    }
    close(xmpcr_fd);

xmpcr_signal_info_exit:
    pthread_mutex_unlock( &channelMutex );
    return;
}

/*========================================================================
 * Name:   xmpcr_set_channel
 * Prototype:  int xmpcr_set_channel( void )
 *
 * Description:
 * Tune to the specified channel.  This sets the active channel.
 *========================================================================*/
void
xmpcr_set_channel( int num )
{
    unsigned char   channel[12] = {
        0x5A, 0xA5, 0x00, 0x06, 0x10, 0x02, 0x00, 0x00, 0x00, 0x01, 0xED, 0xED };

    piboxLogger(LOG_INFO, "Entered.\n");
    pthread_mutex_lock( &channelMutex );

    if ( !xmpcr_init( FALSE ) )
    {
        piboxLogger(LOG_ERROR, "Failed to init port.\n");
        goto xmpcr_set_channel_exit;
    }

    channel[6] = (char)num;
    if ( port_write(channel, 12) )
    {
        usleep(1000);
        piboxLogger(LOG_INFO, "Set channel to %d\n", num);
        xmpcr_get_channel_info( num, 1 );
        active_channel = num;
    }
    close(xmpcr_fd);

xmpcr_set_channel_exit:
    pthread_mutex_unlock( &channelMutex );
    return;
}

/*========================================================================
 * Name:   xmpcr_mute
 * Prototype:  int xmpcr_mute( void )
 *
 * Description:
 * Toggle mute.
 *========================================================================*/
void
xmpcr_mute( void )
{
    unsigned char   channel[8] = { 0x5A, 0xA5, 0x00, 0x02, 0x13, 0x01, 0xED, 0xED };

    piboxLogger(LOG_INFO, "Entered.\n");
    pthread_mutex_lock( &channelMutex );

    if ( !xmpcr_init( FALSE ) )
    {
        piboxLogger(LOG_ERROR, "Failed to init port.\n");
        goto xmpcr_mute_exit;
    }

    mute = !mute;
    channel[5] = (mute)?0x01:0x00;
    if ( port_write(channel, 8) )
    {
        piboxLogger(LOG_INFO, "Set mute to %s\n", (mute)?"On":"Off");
        get_response(0x00);
    }
    else
    {
        mute = !mute;
    }
    close(xmpcr_fd);

xmpcr_mute_exit:
    pthread_mutex_unlock( &channelMutex );
    return;
}

/*========================================================================
 * Name:   dump_channel
 * Prototype:  void dump_channel( void )
 *
 * Description:
 * 
 *========================================================================*/
void
dump_channels( void )
{
    piboxLogger(LOG_INFO, "Entered.\n");
    int i;
    for(i=0; i<32; i++)
    {
        if ( 0 == (int)channel_list[i].number )
            continue;
        piboxLogger(LOG_INFO, "Channel num: %d\n", (int)channel_list[i].number);
        if ( channel_list[i].name != NULL )
            piboxLogger(LOG_INFO, "Station    : %s\n", channel_list[i].name);
        if ( channel_list[i].category != NULL )
            piboxLogger(LOG_INFO, "category   : %s\n", channel_list[i].category);
        if ( channel_list[i].artist != NULL )
            piboxLogger(LOG_INFO, "Artist     : %s\n", channel_list[i].artist);
        if ( channel_list[i].title != NULL )
            piboxLogger(LOG_INFO, "Title      : %s\n", channel_list[i].title);
    }
}

/*========================================================================
 * Name:   xmpcr_get_active_channel
 * Prototype:  void xmpcr_get_active_channel( void )
 *
 * Description:
 * Returns the currently active (re: selected) channel or -1 if no channel
 * has yet been selected.
 *========================================================================*/
int
xmpcr_get_active_channel( void )
{
    piboxLogger(LOG_INFO, "Entered.\n");
    return active_channel;
}

/*========================================================================
 * Name:   xmpcr_reset
 * Prototype:  void xmpcr_reset( void )
 *
 * Description:
 * Reset the port.  This closes and reopens the port.
 *========================================================================*/
void
xmpcr_reset( void )
{
    piboxLogger(LOG_INFO, "Entered.\n");
    pthread_mutex_lock( &channelMutex );
    if ( xmpcr_init( FALSE ) )
        close(xmpcr_fd);
    pthread_mutex_unlock( &channelMutex );
}

/*========================================================================
 * Name:   xmpcr_close
 * Prototype:  void xmpcr_close( void )
 *
 * Description:
 * Close the port.
 *========================================================================*/
void
xmpcr_close( void )
{
    piboxLogger(LOG_INFO, "Entered.\n");
    pthread_mutex_lock( &channelMutex );
    close(xmpcr_fd);
    pthread_mutex_unlock( &channelMutex );
}

